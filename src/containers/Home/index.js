/** @format */

// @flow
/**
 * Created by InspireUI on 19/02/2017.
 */
import React, { PureComponent, Component } from "react";
import PropTypes from "prop-types";
import { View, ImageBackground , Text, Image, ScrollView, StyleSheet, Dimensions } from "react-native";
import { connect } from "react-redux";
import { Constants, withTheme, AppConfig } from "@common";
import { HorizonList, ModalLayout, PostList } from "@components";
// import Carousel from "react-native-snap-carousel";
import styles from "./styles";
class Carousel extends Component {
  render() {
    const { images } = this.props;
    if (images && images.length) {
      return (
        <View
          style={styles.scrollContainer}
        >
          <ScrollView
            horizontal
            pagingEnabled
            showsHorizontalScrollIndicator={false}
          >
            {images.map(image => (
              <Image style={styles.image} source={image.source} />
            ))}
          </ScrollView>
        </View>
      );
    }
    console.log('Please provide images');
    return null;    
  }
}

class Home extends PureComponent {
  static propTypes = {
    fetchAllCountries: PropTypes.func.isRequired,
    layoutHome: PropTypes.any,
    onViewProductScreen: PropTypes.func,
    onShowAll: PropTypes.func,
    showCategoriesScreen: PropTypes.func,
  };

  componentDidMount() {
    const { fetchAllCountries, fetchCategories, isConnected } = this.props;
    if (isConnected) {
      fetchAllCountries();
      fetchCategories();
    }
  }

  render() {
    const {
      layoutHome,
      onViewProductScreen,
      showCategoriesScreen,
      onShowAll,
      theme: {
        colors: { background },
      },
    } = this.props;

    const isHorizontal = layoutHome == Constants.Layout.horizon || layoutHome == 7;
    const images = [
      {
        source: {
          uri: 'https://cdn.pixabay.com/photo/2017/05/19/07/34/teacup-2325722__340.jpg',
        },
      },
      {
        source: {
          uri: 'https://cdn.pixabay.com/photo/2017/05/02/22/43/mushroom-2279558__340.jpg',
        },
      },
      {
        source: {
          uri: 'https://cdn.pixabay.com/photo/2017/05/18/21/54/tower-bridge-2324875__340.jpg',
        },
      },
      {
        source: {
          uri: 'https://cdn.pixabay.com/photo/2017/05/16/21/24/gorilla-2318998__340.jpg',
        },
      },
      
    ];
    return (
      <View style={[styles.container, { backgroundColor: background }]}>
       
        <ImageBackground source={require('./../../images/body2.jpg')} style={{width: '100%', height: '100%'}}>
           {/* {isHorizontal && ( */}
            <View style={styles.container}>
              <Carousel images={images} />
            </View>
          <HorizonList
            onShowAll={onShowAll}
            onViewProductScreen={onViewProductScreen}
            showCategoriesScreen={showCategoriesScreen}
          />
        {/* )} */}

        {/* {!isHorizontal && ( */}
          {/* <PostList onViewProductScreen={onViewProductScreen} /> */}
        {/* )} */}
        <ModalLayout />
        </ImageBackground>
      </View>
    );
  }
}

const mapStateToProps = ({ user, products, countries, netInfo }) => ({
  user,
  layoutHome: products.layoutHome,
  countries,
  isConnected: netInfo.isConnected,
});

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const CountryRedux = require("@redux/CountryRedux");
  const { actions } = require("@redux/CategoryRedux");

  return {
    ...ownProps,
    ...stateProps,
    fetchCategories: () => actions.fetchCategories(dispatch),
    fetchAllCountries: () => CountryRedux.actions.fetchAllCountries(dispatch),
  };
}

export default withTheme(
  connect(
    mapStateToProps,
    undefined,
    mergeProps
  )(Home)
);
