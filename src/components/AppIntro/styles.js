/** @format */

export default {
  mainContent: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: "#CC0442"
  },
  image: {
    width: '100%',
    position: 'absolute',
    top: -100,
  },
  title: {
    fontSize: 22,
    color: '#464D53',
    textAlign: 'center',
    zIndex: 999,
    marginBottom: 250,
  },
  button: {
    backgroundColor: '#fff',
    color: '#464D53',
  },
  image1: {
    width: 320,
    height: 320,
  },
  text: {
    color: "rgba(255, 255, 255, 0.8)",
    backgroundColor: "transparent",
    textAlign: "center",
    paddingHorizontal: 16,
  },
  title1: {
    fontSize: 22,
    color: "white",
    backgroundColor: "transparent",
    textAlign: "center",
    marginBottom: 16,
  },
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: "rgba(0, 0, 0, .2)",
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
  },
};
